//
// Created by Eric Williams on 2019-06-18.
//


#include <jni.h>

extern "C"
{
void Java_nativelab_co_lamelibrary_Mp3Encoder_close(JNIEnv *env, jclass type);

jint Java_nativelab_co_lamelibrary_Mp3Encoder_encode(JNIEnv *env, jclass type, jshortArray buffer_l_,
                                             jshortArray buffer_r_, jint samples,
                                             jbyteArray mp3buf_);

jint Java_nativelab_co_lamelibrary_Mp3Encoder_flush(JNIEnv *env, jclass type, jbyteArray mp3buf_);

void Java_nativelab_co_lamelibrary_Mp3Encoder_init(JNIEnv *env, jclass type, jint inSampleRate,
                                                  jint outChannel, jint outSampleRate,
                                                  jint outBitrate, jint quality);
}
